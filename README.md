# rtmididrv

Please note that rtmididrv has moved to the main repository at gitlab.com/gomidi/midi/v2/drivers/rtmididrv
All further development will be done there.
